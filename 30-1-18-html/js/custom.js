equalheight = function(container){

    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $(container).each(function() {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}

$(window).load(function() {
    equalheight('.product');
});

$(window).resize(function(){
    equalheight('.product');
});

$(window).load(function() {
    equalheight('.eual_pro');
});

$(window).resize(function(){
    equalheight('.eual_pro');
});

//----------- owl carousel
$(document).ready(function() {
    $('#pro_carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        responsiveClass: true,
        stagePadding: 5,
        navText:["&nbsp;","&nbsp;"],
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 4,
                loop: false,
                margin: 20
            }
        }
    });
    $('#mobile_carsouel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        navText:["&nbsp;","&nbsp;"],
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
                loop: false,
                margin: 20
            }
        }
    });
    $('#gal_thumbs').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        navText:["&nbsp;","&nbsp;"],
        nav: true,
        responsive: {
            0: {
                items: 3,

            },
            600: {
                items: 4,
            },
            1000: {
                items: 4,
                loop: false,
                margin: 20
            }
        }
    });
});

//----------- Checkbox Multiple Select
$(document).ready(function() {
    $('#brands').multiselect({
        nonSelectedText: 'Select Brands'
    });
    $('#product_cat').multiselect({
        nonSelectedText: 'Select Products'
    });
});

//----------- Price Filter
$( function() {
    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 75, 300 ],
        slide: function( event, ui ) {
            $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
        " - $" + $( "#slider-range" ).slider( "values", 1 ) );
} );


//----------- product Zoom
$(document).ready(function(){

    $('#gallery').simplegallery({
        galltime : 400,
        gallcontent: '.content',
        gallthumbnail: '.thumbnail',
        gallthumb: '.thumb'
    });

});
//----------- Quantity Inputs
$(document).ready(function(){

    var quantitiy=0;
    $('.quantity-right-plus').click(function(e){

        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());

        // If is not undefined

        $('#quantity').val(quantity + 1);


        // Increment

    });

    $('.quantity-left-minus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());

        // If is not undefined

        // Increment
        if(quantity>0){
            $('#quantity').val(quantity - 1);
        }
    });

});


$(document).ready(function() {
    $('#table').basictable();

    $('#table-breakpoint').basictable({
        breakpoint: 768
    });
});